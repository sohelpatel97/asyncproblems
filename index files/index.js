import fetch from 'node-fetch';
import fs from 'fs';
import axios from 'axios';


//1.Write a function to fetch list of all todos from the above url using both fetch and axios.get.

//A Feth and Axios

let solutionA, solutionB, solutionC
const FetchData = async () => {

  let userData = await fetch('https://jsonplaceholder.typicode.com/todos');
  return userData
}



try {
  let userData = await FetchData()
  let Data = await userData.json()

  //   let res = JSON.stringify(Data)

  //   fs.writeFile('./solutionA.json',res, 'utf8', function (err) {
  //     if (err) {
  //         console.log("An error occured while Making JSON Object to File.");
  //         return console.log(err);
  //     }

  // });

  let solutionA = Data // solutionA
}
catch (e) {
  console.log(e);
}



//axios

const AxiosGetData =async ()=>{

  result = await axios.get("https://jsonplaceholder.typicode.com/todos")

  return result.data

}

try {
  let userData = await AxiosGetData()

  //console.log(userData);

}
catch (e) {
  console.log(e);
}

//B
async function geDataB() {

  let responce = await fetch('https://jsonplaceholder.typicode.com/todos')
  let Jsondata = await responce.json()



  return Jsondata.reduce((userData, curr) => {

    if (!userData.hasOwnProperty("userId" + curr.userId)) {

      userData["userId" + curr.userId] = [curr]

    }
    else {
      userData["userId" + curr.userId].push(curr)
    }


    return userData
  }, {})
}

try {

  var result = await geDataB();

  let todosData = {}

  Object.keys(result).forEach((key) => {

    let value = result[key].sort((a, b) => a.completed - b.completed)

    todosData[key] = value
  })

  solutionB = todosData;

  //   let res = JSON.stringify(todosData)

  //   fs.writeFile('./solutionB.json',res, 'utf8', function (err) {
  //     if (err) {
  //         console.log("An error occured while Making JSON Object to File.");
  //         return console.log(err);
  //     }

  // });


}
catch (err) {
  console.log(err, " too bad for You Error occurred");
}

//C
async function geDataC() {

  let responce = await fetch('https://jsonplaceholder.typicode.com/todos')
  let JsonData = await responce.json()

  return JsonData.reduce((taskData, curr) => {

    if (taskData.hasOwnProperty("completed") && taskData.hasOwnProperty("Incompleted")) {

      if (curr.completed) {
        taskData["completed"].push(curr)
      }
      else {
        taskData["Incompleted"].push(curr)
      }
    } else {

      taskData["completed"] = []
      taskData["Incompleted"] = []

      if (curr.completed) {
        taskData["completed"].push(curr)
      }
      else {
        taskData["Incompleted"].push(curr)
      }
    }

    return taskData

  }, {})
}

try {
  let res = await geDataC();
  solutionC = res

  //   res = JSON.stringify(res)
  //     fs.writeFile('./solutionC.json',res, 'utf8', function (err) {
  //     if (err) {
  //         console.log("An error occured while Making JSON Object to File.");
  //         return console.log(err);
  //     }

  // });

}
catch (e) {
  console.log("error occurred: " + e);
}



let SolutionsResult = {

  A: solutionA,
  B: solutionB,
  c: solutionC,

}


//Q3. Write a function to get all the users information from the users API url. Find all the users with name "Nicholas". 

//Get all the to-dos for those user ids.

async function getNicolasData() {

  let userRes = await fetch('https://jsonplaceholder.typicode.com/users');
  let todosRes = await fetch('https://jsonplaceholder.typicode.com/todos');
  let UserData = await userRes.json()
  let todosData = await todosRes.json()

  let result = UserData.reduce((userData, curr) => {

    if ((curr.name.indexOf("Nicholas")) > -1) {

      let ans = todosData.filter((ele) => ele.userId == curr.id)

      userData["userId" + curr.id] = ans
    }

    return userData

  }, {})

  return result
}

// try{

//   let showResult = await getNicolasData()

//   console.log(showResult);
// }
// catch(err){
//   console.log('Ops error! ',err);
// }






//Q.2  Read file 


const ReadFile = (filePath, trasnformerObj) => {

  let transformDataArry = () => {

    return Object.values(trasnformerObj).map((solution) => solution)
  }

  if (filePath && typeof (filePath == "string")) {

    var ReadMyFile = () => {
      return new Promise(function (resolve, reject) {

        fs.readFile(filePath, "utf-8", function (err, data) {
          if (err) {
            let rej = reject(err);
            console.log(rej);
          }
          else {
            let res = resolve(data);
            console.log(res);
          }
        });
      })
    }

  }


  else {
    return "missing file path"
  }

  return { ReadMyFile, transformDataArry }
}

// ReadFile('./lipsum.txt',SolutionsResult).ReadMyFile()
// .then((data)=>console.log(data))
// .catch((e)=>console.log(e))






