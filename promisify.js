const sayHelloWorld = () => {


    return new Promise((resolve, reject) => {

        let flag = window.setTimeout(() => {
            resolve(console.log('Hello World'))
        }, 1000)
    })
}

(async function executeSayHelloWorld() {

    await sayHelloWorld();
    console.log('Hey');
})()
